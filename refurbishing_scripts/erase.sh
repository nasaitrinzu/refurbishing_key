#!/bin/bash

# erase.sh --
#
#   This file allows to erasing target hard disk of a computer :
#     - magnetic disk by Nwipe utility : https://en.wikipedia.org/wiki/Nwipe
#     - Solid-State Drive by HDparm : https://en.wikipedia.org/wiki/Hdparm
#     - NVMe by nvme-cli : https://github.com/linux-nvme/nvme-cli
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was tested and validated on Clonezilla live 3.0.1-8 i686/amd64
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

#DEBUG="true"
#BEEP_OFF="true"
#AUTOMATIC_HALT_MAGNETIC_OFF="true"
AUTOMATIC_HALT_SSD_OFF="true"

DIR_ERASE_LOG=Erase_log
NWIPE_LOG=nwipe_log
HDPARM_LOG=hdparm_log
NVME_LOG=nvme_cli_log
TESTDISK_LOG=testdisk.log

KEY_DRIVE=$(sudo blkid | grep LABEL=\"IMAGES\" | cut  -d ':' -f1)
UEFI_ON=0
SECUREBOOT_ON=0
DATE=`date -u +"%Y-%m-%d-%H-%M"`
LANGUE=$(echo ${LANG} | cut -d _ -f1)

# Configuration de l'effacement le plus rapide
# Configuration of default erasing
NWIPE_METHOD[1]="zero      "
NWIPE_VERIFY[1]="off"
NWIPE_OPTIONS[1]="--autopoweroff --nousb --noblank"
# Configuration de l'effacement par défaut de Nwipe
# Configuring advanced Wipe
NWIPE_METHOD[2]="dodshort  "
NWIPE_VERIFY[2]="last"
NWIPE_OPTIONS[2]="--autopoweroff --nousb"
# Configuration de l'effacement avancé recommendé par la DoD
# Configuring advanced Wipe
NWIPE_METHOD[3]="dod522022m"
NWIPE_VERIFY[3]="last"
NWIPE_OPTIONS[3]="--autopoweroff --nousb"

NUMBER_ERASE_ID=3

ENHANCED_SECURITY_ERASE=""

# Mode d'analyse de Testdisk
TESTDISK_OPTION[1]="analyze"
TESTDISK_OPTION[2]="analyze,list"
TESTDISK_OPTION[3]="analyze,search"

NUMBER_ANALYSE_ID=3

CONFIG_FILE=clone.ini
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Lecture des options de la ligne de commande
# Read command line options
LINE_OPTION=$1

if [ -z ${DEBUG} ] ; then
    # Efface l'affichage précédant
    clear
fi

if [[ ${LINE_OPTION} == "-a" ]] ; then
    AUTOMATIC="true"
fi


# Test Secureboot
if test -d /sys/firmware/efi/ ; then
    if [ "$(which mokutil)" != "" ] ; then
        if [ "$(mokutil --sb-state | grep "SecureBoot enabled")" != "" ] ; then
            SECUREBOOT_ON=1
        fi
    fi
fi

if [ ${LANGUE} = fr ]
then

    message_info_emmabuntus="Clé USB de réemploi par Emmabuntüs (https://emmabuntus.org)"
    message_info_sources="Les sources sont disponibles sur http://usb-reemploi.emmabuntus.org"
    message_identification_usb_ko="Attention : La clé USB avec l'étiquette \"IMAGES\" n'a pas été indentifiée. Taper sur une touche quelconque pour sortir du script"
    message_trouve_usb_avant="Clé USB trouvée sur"
    message_trouve_usb_apres=", mais ce n'est pas son emplacement habituel, ce qui signifie que vous avez plusieurs disques durs sur ce système."
    message_selection_usb="Veuillez entrer le numéro correspondant au disque cible de l'installation :"
    message_mauvais_nombre_disque_dur="Attention : Vous n'avez pas entré un nombre pour selectionner le disque. Taper sur une touche quelconque pour sortir du script"
    message_mauvais_selection_disque_dur="Attention : Vous n'avez pas entré un nombre correspondant à un disque. Taper sur une touche quelconque pour sortir du script"
    message_usb_disque_dur_avant="Clé USB trouvée sur"
    message_usb_disque_dur_apres="et disque sélectionné sur "
    message_type_disque_dur="Magnétique"
    message_mode_effacement="Modes d'effacement prédéfinis pour la clé de réemploi du plus rapide au plus lent :"
    message_methode_effacement="méthode"
    message_verification_effacement="vérification"
    message_options_effacement="options"
    message_utilisateur_effacement="Choix du mode réalisé par l'utilisateur dans l'interface de Nwipe pour avoir accès à l'ensemble des modes"
    message_choix_effacement="Veuillez sélectionner le mode d'effacement désiré ?"
    message_info_operation="L'effacement va être effectué et à la fin de celui-ci l'ordinateur va s'arrêter."
    message_demande_arret="Voulez-vous arrêter automatiquement l'ordinateur à la fin de l'opération d'effacement ? [o/N] : "
    message_info_log_avant="Vous pourrez trouver le résultat de l'opération d'effacement dans le fichier de log :"
    message_info_log_apres="présent sur la partition \"IMAGES\" de la clé de réemploi."
    message_info_effacement_avant="/!\ Attention : voulez vous effacer le disque /dev/"
    message_info_effacement_apres=", toutes les données seront détruites ? [o/N] : "
    message_start_effacement_avance="Lancement de l'effacement mode amélioré"
    message_start_effacement="Lancement de l'effacement mode standard"
    message_stop_effacement="Abandon de l'effacement sécurisé !!!"
    message_effacement_ssd_ok="Disque dur SSD effacé"
    message_effacement_ssd_ko="Disque dur SSD non effacé !!!"
    message_effacement_ssd_ko_frozen="Disque dur SSD non effacé, car dans un état gelé !!!"
    message_disque_gele="Disque SSD dans un état gelé."
    message_mise_veille="Voulez-vous essayer de le débloquer ? Alors appuyez sur la touche \"Entrée\".\nSuite à cela l'ordinateur va se mettre en veille. Au bout de 10 secondes appuyez sur le bouton marche pour le réveiller."
    message_fichier_config_automatique_non_trouve="Fichier de configuration du clonage en mode automatique (${CONFIG_FILE}) non trouvé !!!"
    message_nombre="Saisissez un nombre ci-dessus, puis validez : "
    message_info_disque_ssd="Affiche les informations sur le disque dur SSD"
    message_stop_effacement_nvme_avant="Disque SSD NVMe "
    message_stop_effacement_nvme_apres=" ne supporte pas le formatage !!!"
    message_analyse_disque_avant="Voulez-vous lancer une analyse de recherche de données sur le disque cible ?"
    message_analyse_disque_apres="/!\ Attention : cela peut prendre beaucoup du temps. [o/N] : "
    message_mode_analyse="Modes d'analyse prédéfinis pour la clé de réemploi du plus rapide au plus lent :"
    message_utilisateur_analyse="Choix du mode réalisé par l'utilisateur dans l'interface de TestDisk pour avoir accès à l'ensemble des modes"
    message_choix_analyse="Veuillez sélectionner le mode d'analyse désiré ?"
    message_resultat_analyse_disque="Résultat de l'analyse du disque cible :"
    message_pause_continue_script="Appuyez sur la touche \"Entrée\" pour continuer le script"
    message_pause_script="Appuyez sur la touche \"Entrée\" pour terminer du script"
    message_stop_computer="Appuyez sur la touche \"Entrée\" pour arrêter l'ordinateur"
    message_start_clonage="Ou appuyer sur la touche \"S\" pour lancer le clonage en mode semi-automatique ou sur \"A\" pour le mode automatique : "
    CODE_OK_MINUSCULE="o"
    CODE_OK_MAJUSCULE="O"

else

    message_info_emmabuntus="Emmabuntüs reusable USB key (https://emmabuntus.org)"
    message_info_sources="Sources are avbailable on http://usb-reemploi.emmabuntus.org"
    message_identification_usb_ko="Warning : The USB key with the \"IMAGES\" label was not found. Press any key to exit the script"
    message_trouve_usb_avant="USB Key found on"
    message_trouve_usb_apres=", but this is not it's usual location, which means that there is several hard drives on your computer."
    message_selection_usb="Please enter the number corresponding to the target disk of this install."
    message_mauvais_nombre_disque_dur="Warning : You did not enter a number to select the disk. Press any key to exit the script"
    message_mauvais_selection_disque_dur="Warning : You did not enter a number corresponding to a disk. Press any key to exit the script"
    message_usb_disque_dur_avant="USB Key found on"
    message_usb_disque_dur_apres="and selected hard drive on "
    message_type_disque_dur="Magnetic"
    message_mode_effacement="Erase modes available from slowest to fastest :"
    message_methode_effacement="method"
    message_verification_effacement="check"
    message_options_effacement="options"
    message_utilisateur_effacement="Choice of mode made by the user in the Nwipe interface to have access to all modes"
    message_choix_effacement="Please select the desired erase mode ?"
    message_info_operation="The erasing will be started and at the end the computer will stop."
    message_demande_arret="Do you want to automatically shut down the computer at the end of the erase operation? [y/N]: "
    message_info_log_avant="You can find the result of the erasing operation in the log file :"
    message_info_log_apres="present on the \"IMAGES\" partition of the key of reuse."
    message_info_effacement_avant="/!\ Warning : Do you want to erase the disk /dev/"
    message_info_effacement_apres=", all data will be destroyed ? [y/N]: "
    message_start_effacement_avance="Start of enhanced mode erasure"
    message_start_effacement="Start of standard mode erasure"
    message_stop_effacement="Erasing aborted !!!"
    message_effacement_ssd_ok="SSD hard drive erased"
    message_effacement_ssd_ko="SSD hard drive not erased  !!!"
    message_effacement_ssd_ko_frozen="SSD hard disk not erased, because in a frozen state !!!"
    message_disque_gele="SSD drive in a frozen state."
    message_mise_veille="Do you want to try to unlock it ? Then press the \"Enter\" key.\nAfter that the computer will go to sleep. After 10 seconds press the power button to wake it up."
    message_fichier_config_automatique_non_trouve="Automatic mode cloning configuration file (${CONFIG_FILE}) not found  !!!"
    message_nombre="Enter a number above, then validate: "
    message_info_disque_ssd="Displays SSD hard drive information"
    message_stop_effacement_nvme_avant="NVMe SSD "
    message_stop_effacement_nvme_apres=" does not support formatting !!!"
    message_analyse_disque_avant="Do you want to run a data search scan on the target disk?"
    message_analyse_disque_apres="/!\ Warning : This operation can take a long time. [y/N]: "
    message_mode_analyse="Predefined analysis modes for the reuse key from fastest to slowest: "
    message_utilisateur_analyse="Choice of mode made by the user in the TestDisk interface to have access to all modes"
    message_choix_analyse="Please select the desired scan mode?"
    message_resultat_analyse_disque="Target Disk scan result:"
    message_pause_continue_script="Press the \"Enter\" key to continue the script"
    message_pause_script="Press the \"Enter\" key to exit"
    message_stop_computer="Appuyez sur la touche \"Entrée\" pour arrêter l'ordinateur"
    message_start_clonage="Or press the \"S\" key to start cloning in semi-automatic mode or \"A\" for automatic mode: "
    CODE_OK_MINUSCULE="y"
    CODE_OK_MAJUSCULE="Y"

fi

function analyse_disque()
{

    file_log=${1}

    echo -e ""
    echo -e "${GREEN}${message_analyse_disque_avant}${NC}"
    echo -e -n "${YELLOW}${message_analyse_disque_apres}${NC}"
    echo -e -n "${ORANGE}"
    read read_analyse
    echo -e -n "${NC}"

    if [ "$read_analyse" = ${CODE_OK_MINUSCULE} ] || [ "$read_analyse" = ${CODE_OK_MAJUSCULE} ] ; then
        if test -f ${TESTDISK_LOG} ; then
            rm ${TESTDISK_LOG}
        fi

        echo -e ""
        echo -e "${message_mode_analyse}"

        for (( analyse_id = 1 ; analyse_id <= ${NUMBER_ANALYSE_ID} ; analyse_id++ ))
        do
            echo -e "${YELLOW}${analyse_id}${NC} - ${GREEN}Mode = ${TESTDISK_OPTION[${analyse_id}]}${NC}"
        done

        erase_id=$((${NUMBER_ANALYSE_ID}+1))
        echo -e "${YELLOW}${analyse_id}${NC} - ${ORANGE}${message_utilisateur_analyse}${NC}"


        echo ""
        echo -e -n "${YELLOW}${message_choix_analyse}${NC} "

        echo -e -n "${ORANGE}"
        read analyse_id_number
        echo -e -n "${NC}"

        if [ ${analyse_id_number} -le ${NUMBER_ANALYSE_ID} ] ; then
            TESTDISK_OPTION=${TESTDISK_OPTION[${analyse_id_number}]}
            TESTDISK_CMD="/cmd"
        else
            TESTDISK_OPTION=""
            TESTDISK_CMD=""
        fi

        sudo testdisk /log ${TESTDISK_CMD} /dev/${HARD_DRIVE} ${TESTDISK_OPTION}

        if test -f ${TESTDISK_LOG} ; then
            echo -e "${GREEN}${message_resultat_analyse_disque}${NC}"
            echo -e ""
            cat ${TESTDISK_LOG}

            echo "" | sudo tee -a ${file_log} >/dev/null
            echo "# TestDisk analyse ################################################################" | sudo tee -a ${file_log} >/dev/null
            echo "" | sudo tee -a ${file_log} >/dev/null
            echo "TESTDISK_CMD=${TESTDISK_CMD}" | sudo tee -a ${file_log} >/dev/null
            echo "TESTDISK_OPTION=${TESTDISK_OPTION}" | sudo tee -a ${file_log} >/dev/null
            echo "" | sudo tee -a ${file_log} >/dev/null
            cat ${TESTDISK_LOG} | sudo tee -a ${file_log} >/dev/null
            rm ${TESTDISK_LOG}

            echo -e ""
            echo -e "${ORANGE}${message_pause_continue_script}${NC}"
            read pause
        fi
    fi

}

echo ""
echo ${message_info_emmabuntus}
echo ${message_info_sources}
echo ""

# Verrouillage du pavé numérique
setleds -D +num

# Positionnement sur la clé USB au niveau des scripts
# Go to scripts top dir
cd /home/partimag

# Identification de la clé USB
# Finding out the USB Key
if [ -z ${KEY_DRIVE} ] ; then

   echo -e -n "${RED}${message_identification_usb_ko}${NC}"
   echo -e "\a"

   read read_no_correct_label_key

   exit 1

elif [ $(echo ${KEY_DRIVE} | grep sdb) ] ; then

    KEY_DRIVE_FOUND="/dev/sdb"

elif [ $(echo ${KEY_DRIVE} | grep sda) ] ; then

    KEY_DRIVE_FOUND="/dev/sda"

fi

# Identification du disque dur
# Finding the hard drive

find_disk=$(sudo fdisk -l | grep 'Disk /dev/sd\|Disk /dev/hd\|Disk /dev/mmcblk\|Disk /dev/hd\|Disk /dev/nvme' | cut -d : -f1 | cut -d " " -f2)
nb_find_disk=$(echo ${find_disk} | wc -w)

if [ ! -z ${DEBUG} ] ; then
    echo -e "KEY_DRIVE_FOUND=${KEY_DRIVE_FOUND}"
    echo -e "find_disk=${find_disk}"
    echo -e "nb_find_disk=${nb_find_disk}"
fi

if [ ${nb_find_disk} -eq 2 ] && [ -n ${KEY_DRIVE_FOUND}  ] ; then # Si 2 disques trouvés et que la clé USB a été identifiée

    for disk_name in ${find_disk}
    do
        if [ "${disk_name}" != ${KEY_DRIVE_FOUND} ] ; then
            HARD_DRIVE=$(echo ${disk_name} | cut -d / -f3)
        fi

    done

elif [ -n "${KEY_DRIVE}" ] ; then

    echo -e "${message_trouve_usb_avant} ${GREEN}${KEY_DRIVE}${NC}${message_trouve_usb_apres}"
    echo ""
    echo "${message_selection_usb}"

    i=0
    for disk_name in ${find_disk}
    do
        if [ ! $(echo ${KEY_DRIVE} | grep ${disk_name}) ] ; then
            disk_model=$(sudo fdisk -l ${disk_name} | grep "Disk model" | cut -d : -f2)
            disk_size=$(sudo fdisk -l ${disk_name} | grep "Disk " | grep sector | cut -d : -f2 | cut -d B -f1)
            i=$(($i+1))
            echo -e "${YELLOW}${i}${NC} - ${GREEN}${disk_name} - model: ${disk_model} - size: ${disk_size}B${NC}"
        fi

    done

    echo ""
    echo -e -n "${YELLOW}${message_nombre}${NC}"

    echo -e -n "${ORANGE}"
    read read_disk_number
    echo -e -n "${NC}"

    # Test si c'est un nombre
    # Check if the entry is a number
    if echo ${read_disk_number} | grep -qE '^[0-9]+$'; then

        echo ""

    else

        echo -e -n "${RED}${message_mauvais_nombre_disque_dur}${NC}"
        echo -e "\a"

        read read_no_correct_number

        exit 2
    fi

    i=0
    macth_number=0

    for disk_name in ${find_disk}
    do
        if [ ! $(echo ${KEY_DRIVE} | grep ${disk_name}) ] ; then
            i=$(($i+1))
            if [ ${read_disk_number} -eq $i ] ; then
                HARD_DRIVE=${disk_name}
                macth_number=1
            fi
        fi
    done

    if [  ${macth_number} -eq 1 ] ; then

        HARD_DRIVE=$(echo ${HARD_DRIVE} | cut -d / -f3)

    else

        echo -e -n "${RED}${message_mauvais_selection_disque_dur}${NC}"
        echo -e "\a"

        read read_no_correct_number

        exit 3

    fi

fi

# Identification du type de disque dur : Classique ou SSD
TYPE_HARD_DRIVE=$(cat /sys/block/${HARD_DRIVE}/queue/rotational)
if [ ${TYPE_HARD_DRIVE} -eq 0 ] ; then

    # Test si le SSD est NVMe
    HARD_DRIVE_NVME=$(sudo nvme list | grep "/dev/" | cut -d " " -f1 )

    if [ "${HARD_DRIVE_NVME}" != "" ] ; then

        # Test support formatage NVMe
        support_formatage_nvme=$(sudo nvme id-ctrl -H ${HARD_DRIVE_NVME} | grep "Format NVM Support")

        # Formatage NVMe non supportée
        if [ "${support_formatage_nvme}" == "" ] ; then

            echo ""
            echo -e -n "${RED}${message_stop_effacement_nvme_avant}${HARD_DRIVE_NVME}${message_stop_effacement_nvme_apres}${NC}"

            echo -e "\a"

            read read_no_support_formatage_nvme

            exit 4

        fi

        NAME_TYPE_HARD_DRIVE="MVMe"
        ERASE_LOG=${NVME_LOG}

    else

        NAME_TYPE_HARD_DRIVE="SSD"
        ERASE_LOG=${HDPARM_LOG}

    fi

else
    # Disque magnétique
    NAME_TYPE_HARD_DRIVE=${message_type_disque_dur}
    ERASE_LOG=${NWIPE_LOG}
fi

echo -e "${message_usb_disque_dur_avant} ${GREEN}${KEY_DRIVE}${NC} ${message_usb_disque_dur_apres}${GREEN}/dev/${HARD_DRIVE}${NC} (Type : ${GREEN}${NAME_TYPE_HARD_DRIVE}${NC})${NC}"
echo ""

# Changement du nom du fichier de log pour savoir si le PC est en UFEI, SecureBoot
if test -d /sys/firmware/efi/ ; then

    if [ "$(mokutil --sb-state | grep "SecureBoot enabled")" != "" ] ; then
        ERASE_LOG=${ERASE_LOG}_UEFI_SB
    else
        ERASE_LOG=${ERASE_LOG}_UEFI
    fi

else
    ERASE_LOG=${ERASE_LOG}_LEGACY
fi

# Détermination du type de CPU
if [ $(uname -m) = "x86_64" ] ; then
    ERASE_LOG=${ERASE_LOG}_X64
else
    ERASE_LOG=${ERASE_LOG}_X32
fi

# Verification si le répetoire de log n'existe pas et création
if ! test -d ${DIR_ERASE_LOG} ; then
    sudo mkdir ${DIR_ERASE_LOG}
fi

# Ajout du répertoire, de la date et de l'extension au fichier de log
ERASE_LOG=${DIR_ERASE_LOG}/${DATE}-${ERASE_LOG}.log

# Effacement pour un disque dur magnétique utilisant Nwipe
if ! [ ${TYPE_HARD_DRIVE} -eq 0 ] ; then

    # Choix du type d'effacement

    echo -e "${message_mode_effacement}"

    for (( erase_id = 1 ; erase_id <= ${NUMBER_ERASE_ID} ; erase_id++ ))
    do
        echo -e "${YELLOW}${erase_id}${NC} - ${GREEN}${message_methode_effacement} = ${NWIPE_METHOD[${erase_id}]},\t ${message_verification_effacement} = ${NWIPE_VERIFY[${erase_id}]},\t ${message_options_effacement} = ${NWIPE_OPTIONS[${erase_id}]}${NC}"
    done

    erase_id=$((${NUMBER_ERASE_ID}+1))
    echo -e "${YELLOW}${erase_id}${NC} - ${ORANGE}${message_utilisateur_effacement}"

    echo ""
    echo -e -n "${YELLOW}${message_choix_effacement}${NC} "

    echo -e -n "${ORANGE}"
    read erase_id_number
    echo -e -n "${NC}"

    if [ ${erase_id_number} -le ${NUMBER_ERASE_ID} ] ; then
        NWIPE_METHOD=${NWIPE_METHOD[${erase_id_number}]}
        NWIPE_VERIFY=${NWIPE_VERIFY[${erase_id_number}]}
        NWIPE_OPTIONS=${NWIPE_OPTIONS[${erase_id_number}]}
    else
        NWIPE_METHOD=""
        NWIPE_VERIFY=""
        NWIPE_OPTIONS=""
    fi

    # Affiche information fichier de log
    echo ""
    echo -e "${message_info_log_avant}"
    echo -e "\"${GREEN}${ERASE_LOG}${NC}\" ${message_info_log_apres}"
    echo ""

    if [ -z ${AUTOMATIC_HALT_MAGNETIC_OFF} ] ; then

        echo -e -n "${YELLOW}${message_demande_arret}${NC}"

        echo -e -n "${ORANGE}"
        read read_arret
        echo -e -n "${NC}"

        if [ "$read_arret" = ${CODE_OK_MINUSCULE} ] || [ "$read_arret" =  ${CODE_OK_MAJUSCULE} ] ;  then
            AUTOMATIC_HALT_MAGNETIC_OFF=""
            echo ""
            echo -e  "${message_info_operation}"
        else
            AUTOMATIC_HALT_MAGNETIC_OFF="true"
        fi

    fi

    # Partie active du script
    # The active part of the script

    # Demande de confirmation de l'effacement
    echo ""
    echo -e -n "${YELLOW}${message_info_effacement_avant}${HARD_DRIVE}${message_info_effacement_apres}${NC}"

    echo -e -n "${ORANGE}"
    read read_1
    echo -e -n "${NC}"

    if [ "$read_1" = ${CODE_OK_MINUSCULE} ] || [ "$read_1" = ${CODE_OK_MAJUSCULE} ] ; then

        echo -e -n "${YELLOW}${message_info_effacement_avant}${HARD_DRIVE}${message_info_effacement_apres}${NC}"

        echo -e -n "${ORANGE}"
        read read_2
        echo -e -n "${NC}"

        if [ "$read_2" = ${CODE_OK_MINUSCULE} ] || [ "$read_2" =  ${CODE_OK_MAJUSCULE} ] ;  then

            if [ "$?" -eq 0 ]; then

                if [[ ${NWIPE_METHOD} != "" ]] ; then
                    sudo nwipe --method=${NWIPE_METHOD} --verify=${NWIPE_VERIFY} ${NWIPE_OPTIONS} --logfile=${ERASE_LOG} --autonuke /dev/${HARD_DRIVE}
                else
                    # Choix réalisé pour l'utilisateur dans l'interface de Nwipe
                    sudo nwipe --logfile=${ERASE_LOG} /dev/${HARD_DRIVE}
                fi

                if [ -z ${BEEP_OFF} ] ; then
                    echo -e -n "\a"
                fi

                if [ ! -z ${AUTOMATIC_HALT_MAGNETIC_OFF} ] ; then

                    shutdown -c

                    analyse_disque ${ERASE_LOG}

                    echo ""
                    echo -e "${ORANGE}${message_stop_computer}${NC}"
                    echo -e -n "${YELLOW}${message_start_clonage}${NC}"
                    echo -e -n "${ORANGE}"
                    read mode_clonage
                    echo -e -n "${NC}"

                    if [ "$mode_clonage" = "s" ] || [ "$mode_clonage" = "S" ] ; then
                        /home/partimag/clone.sh
                    elif [ "$mode_clonage" = "a" ] || [ "$mode_clonage" = "A" ] ; then
                        /home/partimag/clone.sh -a
                    else
                        shutdown --poweroff now
                    fi

                else
                    shutdown --poweroff now
                fi

                exit 0

            fi
        fi
    fi

    echo -e "${RED}${message_stop_effacement}${NC}"
    echo -e -n "\a"

else # Effacement pour un disque dur SSD

    # Disque SSD non NVMe
    if [ "${HARD_DRIVE_NVME}" == "" ] ; then

        # Test si le disque n'est pas gelé
        frozen=$(sudo hdparm -I /dev/${HARD_DRIVE} | grep --perl-regex "not\tfrozen")

    else

        # Pour un disque SSD NVMe forcage du frozen par défaut car impossible de savoir si le disque SSD NVMe est frozen ou pas
        frozen=""

    fi

    # SSD not frozen ####################################################
    if [ "${frozen}" != "" ] ; then

        # Affiche information fichier de log
        echo ""
        if [ -z ${AUTOMATIC_HALT_SSD_OFF} ] ; then
            echo -e    "${message_info_operation}"
        fi
        echo -e "${message_info_log_avant}"
        echo -e "\"${GREEN}${ERASE_LOG}${NC}\" ${message_info_log_apres}"
        echo ""

        # Demande de confirmation de l'effacement
        echo ""
        echo -e -n "${YELLOW}${message_info_effacement_avant}${HARD_DRIVE}${message_info_effacement_apres}${NC}"

        echo -e -n "${ORANGE}"
        read read_1
        echo -e -n "${NC}"

        if [ "$read_1" = ${CODE_OK_MINUSCULE} ] || [ "$read_1" = ${CODE_OK_MAJUSCULE} ] ; then

            echo -e -n "${YELLOW}${message_info_effacement_avant}${HARD_DRIVE}${message_info_effacement_apres}${NC}"

            echo -e -n "${ORANGE}"
            read read_2
            echo -e -n "${NC}"

            if [ "$read_2" = ${CODE_OK_MINUSCULE} ] || [ "$read_2" =  ${CODE_OK_MAJUSCULE} ] ;  then

                if [ "$?" -eq 0 ]; then

                    # Création du fichier de log
                    echo "# Initial state before erasing ########################################################################" > ${ERASE_LOG} >/dev/null
                    sudo hdparm -I /dev/${HARD_DRIVE} | sudo tee -a ${ERASE_LOG} >/dev/null

                    # Utilisation d'un mot de passe pour pouvoir effacer le disque dur
                    sudo hdparm --user-master u --security-set-pass avenir /dev/${HARD_DRIVE}

                    # Test du support du mode Enhanced
                    test_mode_enhanced=$(sudo hdparm -I /dev/${HARD_DRIVE} | grep "supported: enhanced erase")
                    if [ "${test_mode_enhanced}" != "" ] ; then
                        ENHANCED_SECURITY_ERASE="-enhanced"
                        echo "# Erasing mode enhanced ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                        echo ""
                        echo -e "${GREEN}${message_start_effacement_avance}${NC}"
                    else
                        echo "# Erasing mode simple   ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                        echo ""
                        echo -e "${GREEN}${message_start_effacement}${NC}"
                    fi

                    # Effacement du disque dur avec affichage du temps de l'opération
                    time sudo hdparm --user-master u --security-erase${ENHANCED_SECURITY_ERASE} avenir /dev/${HARD_DRIVE}

                    # Création du fichier de log
                    echo "# Actual state after erasing ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                    sudo hdparm -I /dev/${HARD_DRIVE} | sudo tee -a ${ERASE_LOG} >/dev/null

                    echo ""

                    # Test si le disque SSD ne contient pas données
                    # test_disk_ssd=$(sudo dd if=/dev/${HARD_DRIVE} bs=1M count=5 2> /dev/null)
                    read -r test_disk_ssd < <(sudo dd if=/dev/${HARD_DRIVE} bs=1M count=5 2> /dev/null)

                    echo ""

                    if [ "${test_disk_ssd}" == "" ] ; then
                        echo -e "${GREEN}${message_effacement_ssd_ok}${NC}"
                        echo "# Disk SSD erased ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                        echo "test_disk_ssd=${test_disk_ssd}" | sudo tee -a ${ERASE_LOG} >/dev/null
                    else
                        echo -e "${RED}${message_effacement_ssd_ko}${NC}"
                        echo "# Disk SSD not erased !!! ################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                        echo "test_disk_ssd=${test_disk_ssd}" | sudo tee -a ${ERASE_LOG} >/dev/null
                    fi

                    analyse_disque ${ERASE_LOG}

                    if [ -z ${BEEP_OFF} ] ; then
                        echo -e -n "\a"
                    fi

                    if [ ! -z ${AUTOMATIC_HALT_SSD_OFF} ] ; then
                        echo ""
                        echo -e "${ORANGE}${message_stop_computer}${NC}"

                        echo -e -n "${YELLOW}${message_start_clonage}${NC}"
                        echo -e -n "${ORANGE}"
                        read mode_clonage
                        echo -e -n "${NC}"

                        if [ "$mode_clonage" = "s" ] || [ "$mode_clonage" = "S" ] ; then
                            /home/partimag/clone.sh
                        elif [ "$mode_clonage" = "a" ] || [ "$mode_clonage" = "A" ] ; then
                            /home/partimag/clone.sh -a
                        else
                            shutdown --poweroff now
                        fi

                    else
                        shutdown --poweroff now
                    fi

                    exit 0

                fi
            fi
        fi

        echo "# Script aborted by user ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
        echo -e "${RED}${message_stop_effacement}${NC}"
        echo -e -n "\a"

        echo ""
        echo -e "${ORANGE}${message_pause_script}${NC}"
        read pause

    else
    # SSD frozen ####################################################

        # Création du fichier de log
        echo "# Frozen disk ########################################################################" > ${ERASE_LOG}

        # Disque SSD non NVMe
        if [ "${HARD_DRIVE_NVME}" == "" ] ; then
            sudo hdparm -I /dev/${HARD_DRIVE} | sudo tee -a ${ERASE_LOG} >/dev/null
        else
            echo "" | sudo tee -a ${ERASE_LOG} >/dev/null
            sudo nvme list | sudo tee -a ${ERASE_LOG} >/dev/null
            echo "" | sudo tee -a ${ERASE_LOG} >/dev/null
            sudo nvme id-ctrl -H ${HARD_DRIVE_NVME} | sudo tee -a ${ERASE_LOG} >/dev/null
            echo "" | sudo tee -a ${ERASE_LOG} >/dev/null
        fi

        echo -e "${RED}${message_disque_gele}${NC}"
        echo ""
        echo -e "${ORANGE}${message_mise_veille}${NC}"
        echo -e -n "\a"
        read pause

        # Mise en veille de l'ordinateur pour tenter de débloquer le disque SSD
        echo -n mem | sudo tee /sys/power/state >/dev/null

        # Disque SSD non NVMe
        if [ "${HARD_DRIVE_NVME}" == "" ] ; then

            # Test si le disque n'est pas gelé
            frozen=$(sudo hdparm -I /dev/${HARD_DRIVE} | grep --perl-regex "not\tfrozen")

        else

            # Pour un disque SSD NVMe forcage de not frozen par défaut car impossible de savoir si le disque SSD NVMe est frozen ou pas
            frozen="not frozen"

        fi

        # SSD not frozen  ###############################
        if [ "${frozen}" != "" ] ; then

            # Affiche information fichier de log
            echo ""
            if [ -z ${AUTOMATIC_HALT_SSD_OFF} ] ; then
                echo -e    "${message_info_operation}"
            fi
            echo -e "${message_info_log_avant}"
            echo -e "\"${GREEN}${ERASE_LOG}${NC}\" ${message_info_log_apres}"
            echo ""

            # Demande de confirmation de l'effacement
            echo ""
            echo -e -n "${YELLOW}${message_info_effacement_avant}${HARD_DRIVE}${message_info_effacement_apres}${NC}"

            echo -e -n "${ORANGE}"
            read read_1
            echo -e -n "${NC}"

            if [ "$read_1" = ${CODE_OK_MINUSCULE} ] || [ "$read_1" = ${CODE_OK_MAJUSCULE} ] ; then

                echo -e -n "${YELLOW}${message_info_effacement_avant}${HARD_DRIVE}${message_info_effacement_apres}${NC}"

                echo -e -n "${ORANGE}"
                read read_2
                echo -e -n "${NC}"

                if [ "$read_2" = ${CODE_OK_MINUSCULE} ] || [ "$read_2" =  ${CODE_OK_MAJUSCULE} ] ;  then

                    if [ "$?" -eq 0 ]; then

                        # Création du fichier de log
                        echo "# Disk restarted before erasing ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null

                        # Disque SSD non NVMe
                        if [ "${HARD_DRIVE_NVME}" == "" ] ; then

                            sudo hdparm -I /dev/${HARD_DRIVE} | sudo tee -a ${ERASE_LOG} >/dev/null

                            # Utilisation d'un mot de passe pour pouvoir effacer le disque dur
                            sudo hdparm --user-master u --security-set-pass avenir /dev/${HARD_DRIVE}

                            # Test du support du mode Enhanced
                            test_mode_enhanced=$(sudo hdparm -I /dev/${HARD_DRIVE} | grep "supported: enhanced erase")

                        else

                            # Test support formatage NVMe sécurisé
                            test_mode_enhanced=$(sudo nvme id-ctrl -H ${HARD_DRIVE_NVME} | grep "Crypto Erase Support")

                        fi

                        if [ "${test_mode_enhanced}" != "" ] ; then
                            ENHANCED_SECURITY_ERASE="-enhanced"
                            echo "# Erasing mode enhanced ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                            echo ""
                            echo -e "${GREEN}${message_start_effacement_avance}${NC}"
                        else
                            echo "# Erasing mode simple   ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                            echo ""
                            echo -e "${GREEN}${message_start_effacement}${NC}"

                        fi

                        # Disque SSD non NVMe
                        if [ "${HARD_DRIVE_NVME}" == "" ] ; then

                            # Effacement du disque SSD avec affichage du temps de l'opération
                            time sudo hdparm --user-master u --security-erase${ENHANCED_SECURITY_ERASE} avenir /dev/${HARD_DRIVE}

                            # Création du fichier de log
                            echo "# Actual state after erasing ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                            sudo hdparm -I /dev/${HARD_DRIVE} | sudo tee -a ${ERASE_LOG} >/dev/null

                        else

                            # Effacement du disque SSD NVMe avec affichage du temps de l'opération
                            if [ "${test_mode_enhanced}" == "" ] ; then
                                time sudo nvme format ${HARD_DRIVE_NVME}
                            else
                                time sudo nvme format ${HARD_DRIVE_NVME} --ses=1
                            fi

                            # Création du fichier de log
                            echo "# Actual state after erasing ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                            echo "" | sudo tee -a ${ERASE_LOG} >/dev/null
                            sudo nvme list | sudo tee -a ${ERASE_LOG} >/dev/null
                            echo "" | sudo tee -a ${ERASE_LOG} >/dev/null

                        fi

                        # Test si le disque SSD ne contient pas données
                        # test_disk_ssd=$(sudo dd if=/dev/${HARD_DRIVE} bs=1M count=5 2> /dev/null)
                        read -r test_disk_ssd < <(sudo dd if=/dev/${HARD_DRIVE} bs=1M count=5 2> /dev/null)

                        echo ""

                        if [ "${test_disk_ssd}" == "" ] ; then
                            echo -e "${GREEN}${message_effacement_ssd_ok}${NC}"
                            echo "# Disk SSD erased ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                            echo "test_disk_ssd=${test_disk_ssd}" | sudo tee -a ${ERASE_LOG} >/dev/null
                        else
                            echo -e "${RED}${message_effacement_ssd_ko}${NC}"
                            echo "# Disk SSD not erased !!! ################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
                            echo "test_disk_ssd=${test_disk_ssd}" | sudo tee -a ${ERASE_LOG} >/dev/null
                        fi

                        analyse_disque ${ERASE_LOG}

                        if [ -z ${BEEP_OFF} ] ; then
                            echo -e -n "\a"
                        fi

                        if [ ! -z ${AUTOMATIC_HALT_SSD_OFF} ] ; then

                            echo ""
                            echo -e "${ORANGE}${message_stop_computer}${NC}"

                            echo -e -n "${YELLOW}${message_start_clonage}${NC}"
                            echo -e -n "${ORANGE}"
                            read mode_clonage
                            echo -e -n "${NC}"

                            if [ "$mode_clonage" = "s" ] || [ "$mode_clonage" = "S" ] ; then
                                /home/partimag/clone.sh
                            elif [ "$mode_clonage" = "a" ] || [ "$mode_clonage" = "A" ] ; then
                                /home/partimag/clone.sh -a
                            else
                                shutdown --poweroff now
                            fi

                        else
                            shutdown --poweroff now
                        fi

                        exit 0

                    fi
                fi
            fi

            echo "# Script aborted by user ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null
            echo -e "${RED}${message_stop_effacement}${NC}"
            echo -e -n "\a"

            echo ""
            echo -e "${ORANGE}${message_pause_script}${NC}"
            read pause

        else

            echo "# Disk restarted but frozen ########################################################################" | sudo tee -a ${ERASE_LOG} >/dev/null

            echo -e "${RED}${message_effacement_ssd_ko_frozen}${NC}"
            echo -e -n "\a"

            echo ""
            echo -e "${ORANGE}${message_info_disque_ssd}${NC}"
            echo -e "${ORANGE}${message_pause_continue_script}${NC}"
            read pause

            cat ${ERASE_LOG} | more -d

            echo ""
            echo -e "${ORANGE}${message_pause_script}${NC}"
            read pause
        fi
    fi
fi

